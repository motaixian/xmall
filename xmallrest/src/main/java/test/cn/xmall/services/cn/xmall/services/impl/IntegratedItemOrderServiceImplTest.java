package test.cn.xmall.services.cn.xmall.services.impl; 

import cn.xmall.models.Order;
import cn.xmall.models.OrderItem;
import cn.xmall.models.OrderShipping;
import cn.xmall.services.cn.xmall.services.impl.IntegratedItemOrderServiceImpl;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import org.springframework.util.Assert;

import java.util.ArrayList;
import java.util.List;

/** 
* IntegratedItemOrderServiceImpl Tester. 
* 
* @author <Authors name>
* @version 1.0 
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-rest.xml")
public class IntegratedItemOrderServiceImplTest {
    @Autowired
    IntegratedItemOrderServiceImpl integratedItemOrderService;

@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: placeAnOrder(String token, Order order, List<OrderItem> orderItemList, OrderShipping orderShipping) 
* 
*/ 
@Test
public void testPlaceAnOrder() throws Exception { 
//TODO: Test goes here...
//    Order order=new Order();
//    order.setPayment("5000");
//    order.setPaymentType(2);
//    order.setStatus(1);
//    order.setShippingName("Express");
//    order.setPostFee("5");
//    order.setUserId(22L);
//
//    List<OrderItem> orderItemList=new ArrayList<>();
//    OrderItem orderItem=new OrderItem();
//    orderItem.setItemId("54545454");
//    orderItem.setPrice(5000L);
//    orderItem.setTitle("Iphone 8");
//    orderItem.setNum(2);
//    orderItem.setTotalFee(10000L);
//    orderItemList.add(orderItem);
//
//    OrderShipping orderShipping=new OrderShipping();
//    orderShipping.setReceiverAddress("长沙");
//    String token="95baad3b-3dfa-438f-a0e0-47f2b742ccc0";
//    //integratedItemOrderService=new IntegratedItemOrderServiceImpl();
//    integratedItemOrderService.placeAnOrder(token,order,orderItemList,orderShipping);

} 


} 
