package cn.xmall.vo;

import cn.xmall.models.Item;

public class ItemVO {
    private Long id;

    private String title;

    private String sellPoint;

    private Long price;

    private Integer num;

    private String image;

    private Long cid;

    public ItemVO(){

    }

    public ItemVO(Builder builder) {
        this.id=builder.id;
        this.title=builder.title;
        this.sellPoint=builder.sellPoint;
        this.price=builder.price;
        this.num= builder.num;
        this.image=builder.image;
    }

    public ItemVO(Item item){
        this.id=item.getId();
        this.title=item.getTitle();
        this.sellPoint=item.getSellPoint();
        this.price=item.getPrice();
        this.num= item.getNum();
        this.image=item.getImage();
    }

    public Long getId() {
        return id;
    }

    public void setId(Long id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getSellPoint() {
        return sellPoint;
    }

    public void setSellPoint(String sellPoint) {
        this.sellPoint = sellPoint;
    }

    public Long getPrice() {
        return price;
    }

    public void setPrice(Long price) {
        this.price = price;
    }

    public Integer getNum() {
        return num;
    }

    public void setNum(Integer num) {
        this.num = num;
    }

    public String getImage() {
        return image;
    }

    public void setImage(String image) {
        this.image = image;
    }

    public Long getCid() {
        return cid;
    }

    public void setCid(Long cid) {
        this.cid = cid;
    }

    static  public class Builder{
        private Long id;

        private String title;

        private String sellPoint;

        private Long price;

        private Integer num;

        private String image;

        private Long cid;

        public Builder withId(Long id){
            this.id=id;
            return this;
        }

        public Builder withTitle(String title){
            this.title=title;
            return this;
        }

        public Builder withSellPoint(String sellPoint){
            this.sellPoint=sellPoint;
            return this;
        }
        public Builder withPrice(Long price){
            this.price=price;
            return this;
        }
        public Builder withNum(Integer num){
            this.num=num;
            return this;
        }
        public Builder withImage(String image){
            this.image=image;
            return this;
        }
        public Builder withCid(Long cid){
            this.cid=cid;
            return this;
        }

        public ItemVO build(){
            return new ItemVO(this);
        }

    }
}
