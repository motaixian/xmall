package cn.xmall.services;

import cn.xmall.models.OrderItem;

import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/20.
 */
public interface ItemOrderItemService {
    List<OrderItem> getListByOrderId(String orderId);
    void createByList(List<OrderItem> orderItemList);
}
