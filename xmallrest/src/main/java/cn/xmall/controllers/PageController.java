package cn.xmall.controllers;

import cn.xmall.vo.ErrorPageInfoEnum;
import org.springframework.stereotype.Controller;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.servlet.ModelAndView;

/**
 * Created by MoTaiXian on 2017/7/17.
 */

@Controller
public class PageController {

    @GetMapping("/")
    public String getIndex(Model model){
        model.addAttribute("msg","test_info");
        return "index";
    }

//    @GetMapping("/{pageName}")
//    public String getPage(@PathVariable("page_name") String pageName){
//        return pageName;
//    }

    @GetMapping("/error")
    public ModelAndView getErrorPage(int Code) {

        ModelAndView mav=new ModelAndView("error");
        ErrorPageInfoEnum error=ErrorPageInfoEnum.valueOf(Code);

        mav.addObject("title", error.getDesc());
        mav.addObject("msg","");
        return mav;
    }
}
