package cn.xmall.caches;

import cn.xmall.models.User;
import com.alibaba.fastjson.JSON;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import javax.annotation.PostConstruct;
import java.util.Date;

/**
 * Created by MoTaiXian on 2017/7/17.
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-dao.xml")
public class UserCacheTest {

    @Autowired
    UserCache userCache;

    @Test
    public void SaveTest(){
//        String token=UUID.randomUUID().toString();
        String token="d9c37f9c-303b-4e42-895c-b9b97ff44cea";
        User user = new User();
        user.setId(222L);
        user.setPassword("sdfafafaf");
        user.setEmail("AAAA@AAAAA.AAAA");
        user.setPhone("135864545322");
        user.setUsername("Test");
        user.setCreated(new Date());
        user.setUpdated(new Date());

        userCache.save(token,user);
    }

    @Test
    public void getByTokenKeyTest(){
        String token="d9c37f9c-303b-4e42-895c-b9b97ff44cea";
        User user=userCache.getByTokenKey(token);
        System.out.println(user.toString());
        System.out.println(JSON.toJSONString(user));
    }

    @Test
    public void deleteByTokenKenTest(){
        String token="d9c37f9c-303b-4e42-895c-b9b97ff44cea";
        userCache.deleteByTokenKey(token);
    }


}
