package cn.xmall.commons.models;

/**
 * Created by MoTaiXian on 2017/7/18.
 */

public class RequestResult {
    private  Integer status;
    private String msg;
    private Object data;
    static final Integer STATUS_OK=200;
    static final Integer STATUS_ERROR=500;
    static final String DEFAUlt_MSG="Unknown Error~!";
    public RequestResult(Integer status, String msg, Object data){
        this.status=status;
        this.msg=msg;
        this.data=data;
    }
    public RequestResult()
    {
        this.status=STATUS_ERROR;
        this.msg=DEFAUlt_MSG;
        this.data=null;
    }

    public static RequestResult ok(){
        return new RequestResult(STATUS_OK,DEFAUlt_MSG,null);
    }
    public static RequestResult ok(String msg){
        return new RequestResult(STATUS_OK,msg,null);
    }

    public static RequestResult ok(String msg, Object data){
        return new RequestResult(STATUS_OK,msg,data);
    }

    public static RequestResult error(){
        return new RequestResult(STATUS_ERROR,DEFAUlt_MSG,null);
    }
    public static RequestResult error(String msg){
        return new RequestResult(STATUS_ERROR,msg,null);
    }
    public static RequestResult error(String msg, Object data){
        return new RequestResult(STATUS_ERROR,msg,data);
    }



    public Integer getStatus() {
        return status;
    }

    public void setStatus(Integer statu) {
        this.status = statu;
    }

    public String getMsg() {
        return msg;
    }

    public void setMsg(String msg) {
        this.msg = msg;
    }

    public Object getData() {
        return data;
    }

    public void setData(Object data) {
        this.data = data;
    }

}
