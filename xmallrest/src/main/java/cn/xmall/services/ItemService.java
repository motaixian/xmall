package cn.xmall.services;

import cn.xmall.commons.models.PageQueryResult;
import cn.xmall.vo.ItemVO;

public interface ItemService {
    ItemVO getInfoById(Long id);
    String getDescById(Long id);
    PageQueryResult<ItemVO> getInfoByCidWithPage(long cid,int page,int rowNum);
}
