package cn.xmall.vo;

public class WxContentItemVO {
    String id;
    String title;
    String price;
    String thumbImgUrl;


    public WxContentItemVO(String id, String title, String price, String thumbImgUrl) {
        this.id = id;
        this.title = title;
        this.price = price;
        this.thumbImgUrl = thumbImgUrl;
    }

    public WxContentItemVO(Builder builder) {
        this.id = builder.id;
        this.title = builder.title;
        this.price = builder.price;
        this.thumbImgUrl = builder.thumbImgUrl;
    }

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public String getThumbImgUrl() {
        return thumbImgUrl;
    }

    public void setThumbImgUrl(String thumbImgUrl) {
        this.thumbImgUrl = thumbImgUrl;
    }

    static  public class Builder{
        String id;
        String title;
        String price;
        String thumbImgUrl;

        public WxContentItemVO.Builder withId(String id){
            this.id=id;
            return this;
        }

        public WxContentItemVO.Builder withTitle(String title){
            this.title=title;
            return this;
        }

        public WxContentItemVO.Builder withPrice(String price){
            this.price=price;
            return this;
        }

        public WxContentItemVO.Builder withThumbImgUrl(String thumbImgUrl){
            this.thumbImgUrl=thumbImgUrl;
            return this;
        }

        public WxContentItemVO build(){
            return new WxContentItemVO(this);
        }
    }
}
