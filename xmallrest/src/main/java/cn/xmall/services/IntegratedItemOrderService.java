package cn.xmall.services;

import cn.xmall.models.Order;
import cn.xmall.models.OrderItem;
import cn.xmall.models.OrderShipping;
import cn.xmall.models.User;

import java.util.List;

public interface IntegratedItemOrderService {
    String placeAnOrder(User user, Order order, List<OrderItem> orderItemList, OrderShipping orderShipping) throws Exception;
}
