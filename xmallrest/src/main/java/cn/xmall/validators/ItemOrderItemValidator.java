package cn.xmall.validators;

import cn.xmall.models.OrderItem;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ItemOrderItemValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return OrderItem.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        try {
            OrderItem orderItem= (OrderItem) target;
            ExValidationUtils.notNull(errors,orderItem,"orderItem is null");
            ExValidationUtils.state(errors,orderItem.getNum()>0,"orderItem num is illegal");
            ExValidationUtils.hasText(errors,orderItem.getItemId(),"Itemid is empty");
        }catch (ExValidationException e){
            return;
        }
    }


}
