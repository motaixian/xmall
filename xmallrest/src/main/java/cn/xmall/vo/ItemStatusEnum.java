package cn.xmall.vo;

public enum ItemStatusEnum {
    NORMAL("正常",(byte)1),INSTOCK("下架",(byte)2),
    DELETED("删除",(byte)3),SELLOUT("售罄",(byte)4);

    private Byte code;
    private String desc;

    private ItemStatusEnum(String desc, Byte code){
        this.desc=desc;
        this.code=code;
    }

    public Byte getCode() {
        return code;
    }

    public String getDesc() {
        return desc;
    }
}
