package cn.xmall.caches.impl;

import cn.xmall.caches.UserCache;
import cn.xmall.models.User;
import cn.xmall.utills.FastJsonRedisSerializer;
import com.alibaba.fastjson.JSON;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.redis.core.RedisOperations;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.ValueOperations;
import org.springframework.stereotype.Repository;
import org.springframework.stereotype.Service;

import javax.annotation.PostConstruct;
import java.util.concurrent.TimeUnit;

/**
 * Created by MoTaiXian on 2017/7/17.
 */
@Repository
public class UserCacheImpl implements UserCache {

    @Autowired
    RedisTemplate<String,User> redisTemplate;
    private final String USER_CACHE_KEY_PREFIX="USER_CACHE:";
    private final long USER_CACHE_TTL_MIN=30L;

    @PostConstruct
    public void initBean(){
        redisTemplate.setValueSerializer(new FastJsonRedisSerializer<User>(User.class));
    }

    @Override
    public void save(String token, User user) {
        ValueOperations<String,User> valueOper = redisTemplate.opsForValue();
        token=USER_CACHE_KEY_PREFIX+token;
        valueOper.set(token,user,USER_CACHE_TTL_MIN, TimeUnit.MINUTES);
    }

    @Override
    public void stageForMinute(String token, User user, long time) {

        ValueOperations<String,User> valueOper = redisTemplate.opsForValue();
        token=USER_CACHE_KEY_PREFIX+token;
        valueOper.set(token,user,time, TimeUnit.MINUTES);
    }

    @Override
    public User getByTokenKey(String token) {
        ValueOperations<String,User> valueOper = redisTemplate.opsForValue();
        token=USER_CACHE_KEY_PREFIX+token;
        return valueOper.get(token);
    }

    public boolean refreshByTokenKey(String token){
        ValueOperations<String,User> valueOper = redisTemplate.opsForValue();
        token=USER_CACHE_KEY_PREFIX+token;
        User user=valueOper.get(token);
        if(user==null){
            return false;
        }
        valueOper.set(token,user,USER_CACHE_TTL_MIN,TimeUnit.MINUTES);
        return true;
    }

    @Override
    public void deleteByTokenKey(String token) {
        ValueOperations<String, User> valueOper = redisTemplate.opsForValue();
        RedisOperations<String,User> redisOper  = valueOper.getOperations();
        token=USER_CACHE_KEY_PREFIX+token;
        redisOper.delete(token);
    }


//    @Override
//    public void save(String token,User user) {
//        ValueOperations<String,String> valueOper = redisTemplate.opsForValue();
//        valueOper.set(token,JSON.toJSONString(user),30, TimeUnit.MINUTES);
//    }
//
//    @Override
//    public User getByTokenKey(String token) {
//        ValueOperations<String,String> valueOper=redisTemplate.opsForValue();
//        String objJson=valueOper.get(token);
//        if(objJson!=null)
//            return JSON.parseObject(objJson,User.class);
//        return null;
//    }
//
//    @Override
//    public void deleteByTokenKey(String token) {
//        ValueOperations<String, String> valueOper = redisTemplate.opsForValue();
//        RedisOperations<String,String> redisOper  = valueOper.getOperations();
//        redisOper.delete(token);
//    }


}
