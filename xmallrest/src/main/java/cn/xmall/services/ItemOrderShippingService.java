package cn.xmall.services;

import cn.xmall.models.OrderShipping;

/**
 * Created by MoTaiXian on 2017/7/20.
 */
public interface ItemOrderShippingService {
    OrderShipping getByOrderId(String orderId);
    void create(OrderShipping orderShipping);
}
