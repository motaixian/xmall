package cn.xmall.caches;

import cn.xmall.models.User;

/**
 * Created by MoTaiXian on 2017/7/17.
 */
public interface UserCache {
    void save(String token,User user);
    void stageForMinute(String token,User user,long time);
    User getByTokenKey(String token);
    void deleteByTokenKey(String token);
    public boolean refreshByTokenKey(String token);
}
