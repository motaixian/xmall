package cn.xmall.commons.models;

import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/20.
 */

public class PageQueryResult<T> {
    private long total;
    private List<T> rows;

    public long getTotal() {
        return total;
    }

    public void setTotal(long total) {
        this.total = total;
    }

    public List<T> getRows() {
        return rows;
    }

    public void setRows(List<T> rows) {
        this.rows = rows;
    }
}

