package cn.xmall.controllers;

import cn.xmall.commons.models.RequestResult;
import cn.xmall.vo.WxContentItemVO;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

import java.util.ArrayList;
import java.util.List;

@Controller
@RequestMapping("/wxContent")
public class WeiXinContentController {

    @GetMapping("/content/{contentName}")
    @ResponseBody
    public RequestResult getContent(@PathVariable("contentName") String contentName){
        List<WxContentItemVO> wxContentItemVOS=new ArrayList<>();
        if(contentName.equals("hot")){
            WxContentItemVO.Builder builder=new WxContentItemVO.Builder();
            WxContentItemVO item=builder.withId("1")
                        .withPrice("20").withThumbImgUrl("")
                        .withTitle("").build();
        }

        return RequestResult.ok("success",wxContentItemVOS);
    }

}
