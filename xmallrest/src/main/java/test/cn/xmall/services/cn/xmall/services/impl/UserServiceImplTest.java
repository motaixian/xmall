package test.cn.xmall.services.cn.xmall.services.impl; 

import cn.xmall.models.User;
import cn.xmall.services.UserService;
import org.junit.Test;
import org.junit.Before; 
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;
import com.alibaba.fastjson.JSON;

/** 
* UserServiceImpl Tester. 
* 
* @author <Authors name>
* @version 1.0 
*/
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-rest.xml")
public class UserServiceImplTest { 
    @Autowired
    UserService userService;
@Before
public void before() throws Exception { 
} 

@After
public void after() throws Exception { 
} 

/** 
* 
* Method: getByUserName(String userName) 
* 
*/ 
@Test
public void testGetByUserName() throws Exception { 
//TODO: Test goes here...
    User user=userService.getByUserName("zhangsan");
    System.out.println(JSON.toJSONString(user));
}

@Test
public void testLoginUser() throws Exception {
    String token=userService.loginUser("tidy1","123");
    System.out.println(token);
}


} 
