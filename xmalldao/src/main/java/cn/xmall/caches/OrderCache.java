package cn.xmall.caches;

/**
 * Created by MoTaiXian on 2017/7/20.
 */
public interface OrderCache {
    public String generateUniqueOrderId(String prefix,String suffix);
}
