package cn.xmall.vo;

public enum PaymentTypeEnum {
    ONLINE(1,"在线支付"),ON_DELIVERY(2,"货到付款");
    private int code;
    private String desc;
    private PaymentTypeEnum(int code,String desc){
        this.code=code;
        this.desc=desc;
    }

    public int getCode(){
        return this.code;
    }

    public String getDesc(){
        return this.desc;
    }
}
