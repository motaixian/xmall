package cn.xmall.validators;

import cn.xmall.models.Order;
import org.springframework.util.Assert;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class ItemOrderValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return Order.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        try {
            Order order=(Order) target;
            ExValidationUtils.notNull(errors,order,"order is null");
            ExValidationUtils.hasText(errors,order.getPayment(),"illegal order payment");
             /*支付类型，1、在线支付，2、货到付款*/
            int paymentType=order.getPaymentType();
            ExValidationUtils.state(errors,paymentType>0 && paymentType<3,"illegal order payment type");
            int statu=order.getStatus();
            /*'状态：1、未付款，2、已付款，3、未发货，4、已发货，5、交易成功，6、交易关闭'*/
            ExValidationUtils.state(errors,statu>0 && statu<7,"illegal order statu");

        } catch (ExValidationException e) {
            return;
        }
    }
}
