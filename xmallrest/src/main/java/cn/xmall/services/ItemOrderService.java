package cn.xmall.services;

import cn.xmall.commons.models.PageQueryResult;
import cn.xmall.models.Order;
import cn.xmall.models.OrderItem;
import cn.xmall.models.OrderShipping;
import cn.xmall.models.User;
import cn.xmall.vo.OrderStatuEnum;

import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/20.
 */
public interface ItemOrderService {
     void create(Order order);
     PageQueryResult<Order> getListByUserWithPage(User user, int page, int rowNum);
     void UpdateStatuById(User currentUser,String orderId, OrderStatuEnum orderStatu);
}
