package cn.xmall.services.cn.xmall.services.impl;

/**
 * Created by MoTaiXian on 2017/7/18.
 */

public class UserServiceException extends Exception {
    public UserServiceException(String msg, Throwable cause){super(msg, cause);}
    public UserServiceException(String msg) {
        super(msg);
    }
}
