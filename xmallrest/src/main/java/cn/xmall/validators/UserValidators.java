package cn.xmall.validators;

import cn.xmall.models.User;
import org.springframework.validation.Errors;
import org.springframework.validation.ValidationUtils;
import org.springframework.validation.Validator;

public class UserValidators implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return User.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        ValidationUtils.rejectIfEmpty(errors, "username", null, "Username is empty.");
        ValidationUtils.rejectIfEmpty(errors,"id",null,"User Id is empty");
    }
}
