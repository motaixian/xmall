package cn.xmall.caches;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DataAccessException;
import org.springframework.data.redis.connection.RedisConnection;
import org.springframework.data.redis.connection.RedisSetCommands;
import org.springframework.data.redis.connection.StringRedisConnection;
import org.springframework.data.redis.core.RedisCallback;
import org.springframework.data.redis.core.RedisTemplate;
import org.springframework.data.redis.core.StringRedisTemplate;
import org.springframework.stereotype.Repository;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/20.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-caches.xml")
public class OrderCacheTest {

//    @Autowired
//    RandNumPool randNumPool;
    @Autowired
    OrderCache orderCache;

//    @Test
//    public void getRandNumFromPoolTest(){
//        String num=randNumPool.getRandNumFromPool();
//        System.out.println(num);
//    }
//
//    @Test
//    public void testRedisTemplate(){
//
//        Object result=stringRedisTemplate.executePipelined(new RedisCallback<Object>() {
//            @Override
//            public Object doInRedis(RedisConnection redisConnection) throws DataAccessException {
//
//                StringRedisConnection stringRedisConnection=(StringRedisConnection) redisConnection;
//                String key="Test";
//                for(int i=0;i<1000;i++)
//                    stringRedisConnection.sAdd("Test",String.valueOf(i));
//                return null;
//            }
//        });
//
//        System.out.println(result);
//    }

    @Test
    public void generateUniqueOrderIdTest(){
        String result=orderCache.generateUniqueOrderId("F","");
        System.out.println(result);
    }
}
