package cn.xmall.controllers;

import cn.xmall.commons.models.PageQueryResult;
import cn.xmall.commons.models.RequestResult;
import cn.xmall.services.ItemService;
import cn.xmall.vo.ItemVO;
import org.apache.commons.lang3.ObjectUtils;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.web.bind.annotation.*;

import java.util.List;

@Controller
@RequestMapping("/item")
public class ItemController {
    @Autowired
    ItemService itemService;

    private static final Logger logger = LoggerFactory.getLogger(ItemController.class);

    @GetMapping("/base")
    @ResponseBody
    public RequestResult getBaseInfo(Long itemid){
        try {
            ItemVO itemVO=itemService.getInfoById(itemid);
            return RequestResult.ok("success",itemVO);
        } catch (Exception e) {
            return RequestResult.error(e.getMessage());
        }
    }

    @GetMapping("/cat")
    @ResponseBody
    public RequestResult getItemInfoByCatId(Long itemCatId, @RequestParam(value = "page",defaultValue = "1") int page,
                                            @RequestParam(value = "rowNum",defaultValue = "10") int rowNum){
        try {
            PageQueryResult<ItemVO> itemVOList=itemService.getInfoByCidWithPage(itemCatId,page,rowNum);
            return RequestResult.ok("success",itemVOList);
        } catch (Exception e) {
            return RequestResult.error(e.getMessage());
        }
    }

    @GetMapping("/desc")
    @ResponseBody
    public String getDescById(Long itemID){
        String desc= null;
        try {
            desc = itemService.getDescById(itemID);
            if(desc==null){
                return "";
            }
        } catch (Exception e) {
            return "";
        }
        return desc;
    }
}
