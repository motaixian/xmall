package cn.xmall.controllers;

import cn.xmall.commons.models.RequestResult;
import cn.xmall.models.User;
import cn.xmall.services.cn.xmall.services.WeiXinUserService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;

@Controller
@RequestMapping("/weixin")
public class WeiXinUserController {
    @Autowired
    WeiXinUserService weiXinUserService;

    @PostMapping("/login")
    @ResponseBody
    public RequestResult doLogin(String code, User user){
        try {
            if(user!=null)
                return RequestResult.error("请勿重复登录");
            System.out.println(code);
            Assert.hasText(code, "code is empty");
            String token=weiXinUserService.login(code);
            return RequestResult.ok("success",token);
        }catch (Exception e){
            return RequestResult.error(e.getMessage());
        }
    }

}
