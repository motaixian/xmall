package cn.xmall.services;


import cn.xmall.models.User;
import org.springframework.transaction.annotation.Transactional;

/**
 * Created by MoTaiXian on 2017/7/17.
 */

public interface UserService {
    User getByUserName(String userName) throws Exception;
    User getByPhone(String phone) throws Exception;
    void registerUser(User user) throws Exception;
    String loginUser(String userName, String passWord) throws Exception;
    void changePassWord(String token,String passWord) throws Exception;
    void updateUserInfo(String token,User user) throws Exception;
    User getLoggedInUserByToken(String token) throws Exception;
}
