package cn.xmall.vo;

import cn.xmall.models.Order;
import cn.xmall.models.OrderItem;
import cn.xmall.models.OrderShipping;

import java.util.List;

public class IntegratedItemOrderVO extends Order {
    List<OrderItem> orderItems;
    OrderShipping orderShipping;

    public List<OrderItem> getOrderItems() {
        return orderItems;
    }

    public void setOrderItems(List<OrderItem> orderItems) {
        this.orderItems = orderItems;
    }

    public OrderShipping getOrderShipping() {
        return orderShipping;
    }

    public void setOrderShipping(OrderShipping orderShipping) {
        this.orderShipping = orderShipping;
    }

}
