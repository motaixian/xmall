package cn.xmall.vo;

import java.util.HashMap;
import java.util.Map;

public enum ErrorPageInfoEnum {
    UNSUPPORT(1,"暂未支持"),
    UNSUPPORT_WEB(2,"暂未支持web端方访问"),
    UNKNOWNERROR(3,"未知错误");

    private static Map<Integer, ErrorPageInfoEnum> codeMap = new HashMap<Integer, ErrorPageInfoEnum>();

    static {
        for(ErrorPageInfoEnum pageInfoEnum:ErrorPageInfoEnum.values()){
            codeMap.put(pageInfoEnum.getCode(),pageInfoEnum);
        }
    }

    private int code;
    private String desc;

    ErrorPageInfoEnum(int code, String desc) {
        this.code = code;
        this.desc = desc;
    }

    public static ErrorPageInfoEnum valueOf(int code){
        ErrorPageInfoEnum errorPageInfoEnum=codeMap.get(code);
        if(errorPageInfoEnum==null){
            return ErrorPageInfoEnum.UNKNOWNERROR;
        }

        return errorPageInfoEnum;
    }

    public int getCode() {
        return code;
    }

    public void setCode(int code) {
        this.code = code;
    }

    public String getDesc() {
        return desc;
    }

    public void setDesc(String desc) {
        this.desc = desc;
    }
}
