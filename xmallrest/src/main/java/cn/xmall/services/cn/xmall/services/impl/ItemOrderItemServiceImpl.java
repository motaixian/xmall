package cn.xmall.services.cn.xmall.services.impl;

import cn.xmall.mappers.OrderItemMapper;
import cn.xmall.models.OrderItem;
import cn.xmall.models.OrderItemExample;
import cn.xmall.services.ItemOrderItemService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/20.
 */
@Service
public class ItemOrderItemServiceImpl implements ItemOrderItemService {
    private static final Logger logger = LoggerFactory.getLogger(ItemOrderItemServiceImpl.class);

    @Autowired
    OrderItemMapper orderItemMapper;

    @Override
    public List<OrderItem> getListByOrderId(String orderId){
        OrderItemExample orderItemExample=new OrderItemExample();
        OrderItemExample.Criteria criteria=orderItemExample.createCriteria();
        criteria.andOrderIdEqualTo(orderId);
        List<OrderItem> orderItemList=orderItemMapper.selectByExample(orderItemExample);

        return orderItemList;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
    public void createByList(List<OrderItem> orderItemList){
        Assert.notEmpty(orderItemList,"illegal orderItemList");
        for(OrderItem orderItem:orderItemList){
            Assert.notNull(orderItem.getOrderId(),"orderItem orderID is null");
            orderItemMapper.insert(orderItem);
        }
    }

    // TODO: 2017/7/20 只验证了对象不为空尚未验证关键字段
    private boolean isValidOrderItem(OrderItem orderItem){
        try {
            Assert.notNull(orderItem,"orderItem is null");
            Assert.notNull(orderItem.getOrderId(),"orderItem orderID is null");
        }catch (Exception e){
            logger.warn(e.getMessage());
            return false;
        }
        return true;
    }

}
