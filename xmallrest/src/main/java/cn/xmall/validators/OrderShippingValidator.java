package cn.xmall.validators;

import cn.xmall.models.OrderShipping;
import org.springframework.validation.Errors;
import org.springframework.validation.Validator;

public class OrderShippingValidator implements Validator {

    @Override
    public boolean supports(Class<?> clazz) {
        return OrderShipping.class.equals(clazz);
    }

    @Override
    public void validate(Object target, Errors errors) {
        try {
            OrderShipping orderShipping = (OrderShipping) target;
            ExValidationUtils.notNull(errors,orderShipping,"orderShipping object is null");
            ExValidationUtils.hasText(errors,orderShipping.getReceiverPhone(),"ReceiverPhone is empty");
            ExValidationUtils.hasText(errors,orderShipping.getReceiverAddress(),"ReceiverAddress is empty");
        }catch (ExValidationException e){
            return;
        }
    }
}
