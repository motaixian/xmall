package cn.xmall.vo;

public enum OrderStatuEnum {
    UNPAID(1,"未付款"),PAID(2,"已付款"),SHIPPED(3,"已发货"),
    UNSHIPPED(4,"未发货"),DEALSUCESS(5,"交易成功"),DEALCANCLE(6,"交易取消");
    private final int code;
    private final String desc;
    private OrderStatuEnum(int code,String desc){
        this.code=code;
        this.desc=desc;
    }

    public int getCode(){
        return this.code;
    }

    public String getDesc(){
        return this.desc;
    }

}
