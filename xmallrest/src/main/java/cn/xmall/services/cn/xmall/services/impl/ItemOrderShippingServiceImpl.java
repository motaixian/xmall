package cn.xmall.services.cn.xmall.services.impl;

import cn.xmall.mappers.OrderShippingMapper;
import cn.xmall.models.OrderShipping;
import cn.xmall.models.OrderShippingExample;
import cn.xmall.services.ItemOrderShippingService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Propagation;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;

import java.util.Date;
import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/20.
 */

@Service
public class ItemOrderShippingServiceImpl implements ItemOrderShippingService{
    private static final Logger logger = LoggerFactory.getLogger(ItemOrderShippingServiceImpl.class);

    @Autowired
    OrderShippingMapper orderShippingMapper;

    @Override
    public OrderShipping getByOrderId(String orderId){
        OrderShippingExample orderShippingExample=new OrderShippingExample();
        OrderShippingExample.Criteria criteria=orderShippingExample.createCriteria();
        criteria.andOrderIdEqualTo(orderId);

        List<OrderShipping> orderShippingList=orderShippingMapper.selectByExample(orderShippingExample);
        if(orderShippingList!=null && orderShippingList.size()>0){
            return orderShippingList.get(0);
        }

        return null;
    }

    @Override
    @Transactional(propagation = Propagation.REQUIRED,rollbackFor = Exception.class)
    public void create(OrderShipping orderShipping){
        //订单货运信息属性是否符合要求
        Assert.notNull(orderShipping,"illegal orderShipping");

        orderShipping.setCreated(new Date());
        orderShipping.setUpdated(new Date());
        orderShippingMapper.insert(orderShipping);
    }


}
