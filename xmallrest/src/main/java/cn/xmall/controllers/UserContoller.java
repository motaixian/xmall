package cn.xmall.controllers;

import cn.xmall.commons.models.RequestResult;
import cn.xmall.commons.utils.CookieUtils;
import cn.xmall.models.User;
import cn.xmall.services.UserService;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.stereotype.Controller;
import org.springframework.stereotype.Repository;
import org.springframework.web.bind.annotation.*;

import javax.annotation.PostConstruct;
import javax.servlet.http.HttpServletRequest;
import javax.servlet.http.HttpServletResponse;

/**
 * Created by MoTaiXian on 2017/7/18.
 */
@Controller
@RequestMapping("/user")
public class UserContoller {
    private static final Logger logger = LoggerFactory.getLogger(UserContoller.class);
    @Autowired
    UserService userService;

    @Value("${MLOGIN_COOKIE_NAME}")
    private String COOKIE_NAME;

    @PostMapping("/check/{checkType}/{checkValue}")
    @ResponseBody
    // TODO: 2017/7/18
    /*  doCheck 检测用户名，密码是否已被使用
     * @param checkType 1 表示用户名，2 表示手机号码
     * @param value 表示检查的值
     * @return 返回请求结果
     */
    public RequestResult doCheck(@PathVariable("checkType") int checkType,@PathVariable("checkValue") String value){
        return RequestResult.ok("success",true);
    }

    @PostMapping("/register")
    @ResponseBody
    public RequestResult doRegister(User user){
        try{
            userService.registerUser(user);
        }catch (Exception e){
            return RequestResult.error("fail to register");
        }
        return RequestResult.ok("success",true);
    }

    @PostMapping("/forgetPassword/{tempToken}")
    @ResponseBody
    public RequestResult doforgetPassword(@PathVariable("tempToken") String tempToken,String passWord) throws Exception {
        try {
            userService.changePassWord(tempToken, passWord);
        }catch (Exception e){
            return RequestResult.error("fail to change password");
        }

        return RequestResult.ok("success",true);
    }

    @PostMapping("/changePassword")
    @ResponseBody
    // TODO: 2017/7/18
    public RequestResult doChangePassword(String passWord){
        return RequestResult.ok();
    }

    /*
    * 登陆如果成功则跳转如果
    *
    * */
    @RequestMapping("/loginCheck")
    @ResponseBody
    public RequestResult doLogin(HttpServletRequest request, HttpServletResponse response) throws Exception {
        try {
            String userName = request.getParameter("userName");
            String password = request.getParameter("password");

            //调用用户服务处理登陆,得到用户Token
            String token = userService.loginUser(userName, password);
            // 如果token为null则登陆失败
            if (token == null) {
                return RequestResult.error("fail to login");

            }
            //设置Cookie,Cookie有效期为关闭浏览器就失效
            CookieUtils.setCookie(request, response, "XMALL_SESSION_ID", token);
            CookieUtils.setCookie(request, response, "UserName", userName);

            return RequestResult.ok("success",token);
        }catch (Exception e){
            return RequestResult.error("Exception"+e.getMessage());
        }
    }


    public String showLoginPage(){
        return null;
    }

    

}
