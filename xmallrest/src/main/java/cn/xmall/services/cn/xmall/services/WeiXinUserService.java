package cn.xmall.services.cn.xmall.services;

import cn.xmall.models.User;
import org.springframework.transaction.annotation.Transactional;

public interface WeiXinUserService {
    @Transactional
    String login(String code) throws Exception;

    void register(String code) throws Exception;

    boolean isRegiteredWxUser(String openId) throws Exception;

    User getLoginedUserByToken(String token);
}
