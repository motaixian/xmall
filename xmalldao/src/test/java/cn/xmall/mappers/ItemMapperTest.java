package cn.xmall.mappers;

import cn.xmall.models.Item;
import cn.xmall.models.ItemExample;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.List;

/**
 * Created by MoTaiXian on 2017/7/17.
 */

@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(locations = "classpath:spring-dao.xml")
public class ItemMapperTest {
    private static final Logger logger = LoggerFactory.getLogger(ItemMapperTest.class);
    @Autowired
    ItemMapper itemMapper;

    @Test
    public void getItemTest(){
        ItemExample itemExample=new ItemExample();
        ItemExample.Criteria criteria =itemExample.createCriteria();

        List<Item> itemList=itemMapper.selectByExample(itemExample);
        logger.info(itemList.get(0).toString());


    }


}
