package cn.xmall.controllers;

import cn.xmall.commons.models.PageQueryResult;
import cn.xmall.commons.models.RequestResult;
import cn.xmall.models.Order;
import cn.xmall.models.OrderShipping;
import cn.xmall.models.User;
import cn.xmall.services.IntegratedItemOrderService;
import cn.xmall.services.ItemOrderService;
import cn.xmall.services.ItemOrderShippingService;
import cn.xmall.vo.IntegratedItemOrderVO;
import cn.xmall.vo.OrderStatuEnum;
import com.alibaba.fastjson.JSON;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.util.Assert;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

@Controller
@RequestMapping("/itemOrder")
public class ItemOrderController {

    private static final Logger logger = LoggerFactory.getLogger(ItemOrderController.class);

    @Autowired
    IntegratedItemOrderService integratedItemOrderService;

    @Autowired
    ItemOrderService itemOrderService;

    @Autowired
    ItemOrderShippingService itemOrderShippingService;


    /**
     * 下定订单
     * @param integratedItemOrderVO
     * @param currentUser
     * @return
     */
    @PostMapping("/create")
    @ResponseBody
    // TODO: 2017/7/24  需要验证ItemOrderVO里的关键字段不为空
    public RequestResult placeAnOrder(@RequestBody IntegratedItemOrderVO integratedItemOrderVO, User currentUser) {
        try {
            Assert.notNull(currentUser,"user is not online");
            System.out.println(JSON.toJSONString(currentUser));
            integratedItemOrderVO.setStatus(OrderStatuEnum.UNPAID.getCode());
            //integratedItemOrderVO.setPaymentType(PaymentType.ONLINE);
            String orderId = integratedItemOrderService.placeAnOrder(currentUser, (Order) integratedItemOrderVO, integratedItemOrderVO.getOrderItems(),
                    integratedItemOrderVO.getOrderShipping());
            return RequestResult.ok("palce an order success", orderId);
        } catch (Exception e) {
            return RequestResult.error("palce an order fail", null);
        }
    }

    /**
     *取消一个订单
     * @param orderId 商品订单号
     * @param currentUser RequestResult
     * @return RequestResult
     */
    @PostMapping("/cancel")
    @ResponseBody
    public RequestResult cancelAnOrder(String orderId,User currentUser) {
        try {
            Assert.notNull(currentUser,"user is not online");
            itemOrderService.UpdateStatuById(currentUser, orderId, OrderStatuEnum.DEALCANCLE);
            return RequestResult.ok("success");
        }catch (Exception e){
            return RequestResult.error();
        }
    }

    /**
     *通过商品订单ID 请求商品货运信息
     * @param orderId 商品订单ID
     * @return RequestResult
     */
    @PostMapping("/getShipping")
    @ResponseBody
    public RequestResult getShipping(String orderId){
        try {
            OrderShipping orderShipping=itemOrderShippingService.getByOrderId(orderId);
            orderShipping.setCreated(null);
            orderShipping.setUpdated(null);
            return RequestResult.ok("success",orderShipping);
        }catch (Exception e){
            return RequestResult.error(e.getMessage());
        }

    }

    /**
     * 分页请求用户订单
     * @param page 请求数据的分页号
     * @param rows 每页请求数据的条数
     * @param currentUser 由WebArgumentResolever注入的当前用户
     * @return
     */
    @PostMapping("/getList")
    @ResponseBody
    public RequestResult getOrderListWithPage(int page, int rows,User currentUser) {
        try{
            Assert.notNull(currentUser,"user is not online");
            PageQueryResult<Order> orderPageQueryResult=itemOrderService.getListByUserWithPage(currentUser,page,rows);
            return RequestResult.ok("success",orderPageQueryResult);
        }catch (Exception e){
            return RequestResult.error(e.getMessage());
        }
    }
}

