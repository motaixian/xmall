package cn.xmall.validators;

public final class ExValidationException extends Exception {

    public ExValidationException(){

    }
    public ExValidationException(String msg){
        super(msg);
    }
}
